package com.company;// - dodać poprawną formę dzielenia - z obsługą floata
// - formatowanie całości (wodotryski i czyszczenie ekranu)

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

class Main {
    static private Message msg = new Message();
    static private ValidateInput checkedInput = new ValidateInput();
    static private Calculations cal = new Calculations();

    static private void getInput(){
        msg.print("Enter first number: ");
        cal.setX(checkedInput.inputValidator());
        msg.print("Enter second number: ");
        cal.setY(checkedInput.inputValidator());
    }

    static private void showResult(){
        Scanner s = new Scanner(System.in);
        msg.print("Result: " + cal.getResult() + "\n");
        msg.print("Press ENTER to continue");
        s.nextLine();
        showMenu();
    }

    static private void showMenu(){
        msg.print("\n" + "Please pick menu option:" +
                "\n 1 - adds two numbers" +
                "\n 2 - subtracts two numbers" +
                "\n 3 - multiplies two numbers" +
                "\n 4 - divides two numbers" +
                "\n 0 - quit" + "\n");
    }
    public static void main(String[] args) {
        int menuControl = 1; // zmienna sterująca menu - sterowana przez użytkownika
        Scanner read = new Scanner(System.in);
        // czysto estetyczne
        msg.print(
                "\n ==================================================="
                        + "\n TURBO CALC 6000" + " by Miniacz"
                        + "\n ==================================================="
        );

        showMenu();
        while (menuControl > 0){ // główna pętla menu - ma powtarzać menu, dopóki użytkownik nie będzie chciał skończyć programu
            menuControl = read.nextInt();

            if (menuControl == 1){
                msg.print(
                        "\n ============ ADD ============"
                                + "\n adds two numbers given by user "
                                + "\n ============================="
                );
                getInput();
                try {
                    cal.addXY("add");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                showResult();

            } else if (menuControl == 2) {
                msg.print(
                        "\n ============ SUBTRACT ============"
                                + "\n subtracts two numbers given by user "
                                + "\n =================================="
                );
                getInput();
                try {
                    cal.addXY("substract");
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                showResult();

            } else if (menuControl == 3) {
                msg.print(
                        "\n ============ MULTIPLY ============"
                                + "\n multiplies two numbers given by user "
                                + "\n =================================="
                );
                getInput();
                try {
                    cal.multiplyXY();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                showResult();

            } else if (menuControl == 4) {
                msg.print(
                        "\n ============ DIVIDE ============"
                                + "\n divides two numbers given by user "
                                + "\n ================================"
                );
                getInput();
                try {
                    cal.divideXY();
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                }
                showResult();

            } else if (menuControl > 4) {
                msg.print("\n ERROR - Wrong value picked. You're in main menu."
                        + "\n Please enter value between 0 up to 4\n");
            }

            if (menuControl == 0){
                msg.print("\n Bye! \n");
            }
        }
    }
}

class Calculations{
    private double x;
    private double y;
    private double result;

    public double getResult() {
        return result;
    }
    public void setX(double x) {
        this.x = x;
    }
    public void setY(double y) {
        this.y = y;
    }

    // == metoda dla dodawania i odejmowania ==
    public double addXY(String sign){
        if (sign == "add"){
            return result = this.x + this.y;
        }
        return result = this.x - this.y;
    }

    // == metoda dla mnożenia ==
    public double multiplyXY(){
        return result = this.x * this.y;
    }

    // == metoda dla dzielenia ==
    public double divideXY() throws IllegalArgumentException{
        if (this.y > 0){
            return result = this.x / this.y;
        }
        throw new IllegalArgumentException("Can't divide by 0");
    }
}

class Message{
    public void print(String msg){
        System.out.println(msg);
    }
}

class ValidateInput {
    static private Message msg = new Message();

    public double inputValidator() {
        BufferedReader userInput = new BufferedReader(new InputStreamReader(System.in));
        double readed = 0;
        boolean czyPoprawne = true;
        int tryCounter = 0;

        while (czyPoprawne) {
            //obsługa wyjątków
            try {
                readed = Double.parseDouble(userInput.readLine());
                czyPoprawne = false;
            } catch (NumberFormatException n) {
                msg.print("Invalid data type. Please use numbers.");

                // zabezpieczenie przed pytaniem użytkownika w nieskończoność
                ++tryCounter;
                if (tryCounter == 3) {
                    czyPoprawne = false;
                    msg.print("\n After 3 unsuccessful trials of getting user input, program stopped asking and used value 0 instead. \n");
                }
            } catch (IOException e) {
                msg.print("Error");
            }
        }
        return readed;
    }
}